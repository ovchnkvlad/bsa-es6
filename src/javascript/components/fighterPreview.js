import { createElement } from "../helpers/domHelper";

export function createFighterPreview(fighter, position) {
  const positionClassName =
    position === "right" ? "fighter-preview___right" : "fighter-preview___left";
  const fighterElement = createElement({
    tagName: "div",
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  const { name, health, attack, defense } = fighter;

  const fighterImg = createFighterImage(fighter);

  const fighterDescription = createElement({
    tagName: "div",
    className: `fighter-preview___description ${positionClassName}`,
  });

  const fighterName = createElement({
    tagName: "p",
    className: `fighter-preview___description-name`,
  });

  const fighterHealth = createElement({
    tagName: "p",
    className: `fighter-preview___description-p`,
  });
  const fighterAttack = createElement({
    tagName: "p",
    className: `fighter-preview___description-p`,
  });
  const fighterDefense = createElement({
    tagName: "p",
    className: `fighter-preview___description-p`,
  });
  fighterName.innerText = `${name}`;
  fighterHealth.innerText = `Health: ${health}`;
  fighterAttack.innerText = `Attack: ${attack}`;
  fighterDefense.innerText = `Defence: ${defense}`;
  fighterDescription.append(
    fighterName,
    fighterHealth,
    fighterAttack,
    fighterDefense
  );
  fighterElement.append(fighterDescription);
  fighterElement.append(fighterImg);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: "img",
    className: "fighter-preview___img",
    attributes,
  });

  return imgElement;
}
