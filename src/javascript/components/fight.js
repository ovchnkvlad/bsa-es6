import { controls } from "../../constants/controls";

export async function fight(firstFighter, secondFighter) {
  const {
    PlayerOneAttack,
    PlayerOneBlock,
    PlayerTwoAttack,
    PlayerTwoBlock,
    PlayerOneCriticalHitCombination,
    PlayerTwoCriticalHitCombination,
  } = controls;

  let firstFighterHealth = firstFighter.health;
  let secondFighterHealth = secondFighter.health;

  function runOnKeys(func, ...codes) {
    let pressed = new Set();
    document.addEventListener("keydown", function (event) {
      pressed.add(event.code);

      for (let code of codes) {
        // все ли клавиши из набора нажаты?
        if (!pressed.has(code)) {
          return;
        }
      }
      // во время показа alert, если посетитель отпустит клавиши - не возникнет keyup
      // при этом JavaScript "пропустит" факт отпускания клавиш, а pressed[keyCode] останется true
      // чтобы избежать "залипания" клавиши -- обнуляем статус всех клавиш, пусть нажимает всё заново
      pressed.clear();
      func();
    });

    document.addEventListener("keyup", function (event) {
      pressed.delete(event.code);
    });
  }

  // first player attack/ second block
  runOnKeys(
    getDamage(firstFighter, secondFighter),
    PlayerOneAttack,
    PlayerTwoBlock
  );
  // Second player attack/ first block
  runOnKeys(
    getDamage(secondFighter, firstFighter),
    PlayerTwoBlock,
    PlayerOneAttack
  );
  // First player attack
  runOnKeys(
    () => secondFighterHealth - getDamage(firstFighter, secondFighter),
    PlayerOneAttack
  );
  // Second player attack
  runOnKeys(
    () => firstFighterHealth - getDamage(secondFighter, firstFighter),
    PlayerOneAttack
  );
  runOnKeys(getDamage(secondFighter, firstFighter), PlayerTwoBlock);
  runOnKeys(() => alert("Привет!"), PlayerOneCriticalHitCombination);
  runOnKeys(() => alert("Привет!"), PlayerTwoCriticalHitCombination);

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  if (damage > 0) {
    return damage;
  }
  return 0;
}

export function getHitPower(fighter) {
  // return hit power
  // getHitPower, который бы рассчитывал силу удара (количество ущерба здоровью противника)
  //по формуле power = attack * criticalHitChance ;, где criticalHitChance - рандомно число от 1 до 2,
  const { attack } = fighter;
  return (power = attack * (Math.random() * 2));
}

export function getBlockPower(fighter) {
  // return block power
  // getBlockPower, который бы рассчитывал силу блока (амортизация удара противника)
  //по формуле power = defense * dodgeChance ;, где dodgeChance - рандомное число от 1 до 2.
  const { defense } = fighter;
  return (power = defense * (Math.random() * 2));
}
